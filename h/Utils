/* Copyright 1997 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**************************************************************/
/* File:    Utils.h                                           */
/*          (C) 2000 Pace Micro Technology PLC                */
/*          All rights reserved                               */
/*                                                            */
/* Purpose: Infrequently altered utilities.                   */
/*                                                            */
/* Author:  A.D.Hodgkinson.                                   */
/*                                                            */
/* History: 18-Oct-1996 (ADH): Created.                       */
/*          07-Apr-2000 (ADH): 64-wide comments adopted.      */
/**************************************************************/

#ifndef Browser_Utils__
  #define Browser_Utils__

  #include <kernel.h>
  #include <tboxlibs/wimp.h>
  #include <tboxlibs/toolbox.h>
  #include <tboxlibs/event.h>

  #include "Global.h"

  /* Known error numbers */

  #define Utils_Error_OS_Escape      17              /* Offers only 'Continue'       */
  #define Utils_Error_Custom_Normal  (1u<<30)        /* Offers 'Continue' and 'Quit' */
  #define Utils_Error_Custom_Fatal   ((1u<<30) + 1)  /* Offers only 'Quit'           */
  #define Utils_Error_Custom_Message ((1u<<30) + 2)  /* Offers only 'Continue'       */

  /* Useful macros */

  #define Beep          (_swi(0x107,0))
  #define Swap(a,b)     { (a) ^= (b); (b) ^= (a); (a) ^= (b); }
  #define StrNCpy0(x,y) {strncpy(x,y,sizeof(x)-1); x[sizeof(x)-1]=0;}
  #define WordAlign(a)  (void *) ((((unsigned int) (a)) + (sizeof(int) - 1)) & (~(sizeof(int) - 1)))

  #define ChkError(e)   (show_error_cont(e))
  #define RetError(fn)  do {_kernel_oserror * e = (fn); if (e) return e;} while (0)
  #define StrLastE      do {(void) utils_kernel_last_error();} while (0)
  #define RetLastE      do {return utils_kernel_last_error();} while (0)
  #define RetWarnE(e)   do {if (&erb != (e)) erb = *(e); erb.errnum = Utils_Error_Custom_Message; return &erb;} while (0)

  /* Function prototypes */

  char              * lookup_token                     (const char * s, int flag, const char * arg);
  char              * lookup_choice                    (const char * s, int flag, const char * arg);
  char              * lookup_control                   (const char * s, int flag, const char * arg);

  void                show_error                       (_kernel_oserror * e);
  void                show_error_cont                  (_kernel_oserror * e);
  void                show_error_ret                   (_kernel_oserror * e);
  int                 show_error_ask                   (_kernel_oserror * e, const char * buttons);
  int                 report_toolbox_error             (int eventcode, ToolboxEvent * event, IdBlock * idb, void * handle);

  #ifdef JAVASCRIPT

    _kernel_oserror * make_no_javascript_memory_error  (int stage);

  #endif

  _kernel_oserror   * make_no_fetch_memory_error       (int stage);
  _kernel_oserror   * make_no_cont_memory_error        (int stage);
  _kernel_oserror   * make_no_table_memory_error       (int stage);
  _kernel_oserror   * make_no_memory_error             (int stage);
  _kernel_oserror   * make_general_error               (const char * token, const char * param);
  _kernel_oserror   * utils_kernel_last_error          (void);

  void                show_centred                     (ObjectId o);
  _kernel_oserror   * set_corrected_extent             (unsigned int f, ObjectId o, BBox * w);
  int                 find_behind                      (int w);
  _kernel_oserror   * find_tool_sizes                  (int * theight, int * hheight, int * vwidth);

  void                register_null_claimant           (int eventcode, WimpEventHandler * handler, browser_data * handle);
  void                deregister_null_claimant         (int eventcode, WimpEventHandler * handler, browser_data * handle);

  int                 utils_intersection               (const BBox * restrict a, const BBox * restrict b, BBox * restrict intersection);
  int                 utils_set_graphics_window        (const BBox * restrict rbox, const BBox * restrict cbox, BBox * restrict ibox);
  void                utils_restore_graphics_window    (const BBox * restrict cbox);

  void                read_os_to_points                (void);
  void                convert_pair_to_os               (int x, int y, int * restrict osx, int * restrict osy);
  void                convert_pair_to_points           (int x, int y, int * restrict mpx, int * restrict mpy);
  void                convert_to_os                    (int x, int * restrict osx);
  void                convert_to_points                (int x, int * restrict mpx);
  void                convert_box_to_os                (const BBox * restrict mp, BBox * restrict os);
  void                convert_box_to_points            (const BBox * restrict os, BBox * restrict mp);

  _kernel_oserror   * utils_read_sprite_size           (const char * restrict name, int * restrict width, int * restrict height);
  _kernel_oserror   * utils_text_width                 (char * text, int * width, int scan);
  _kernel_oserror   * set_gadget_state                 (ObjectId o, ComponentId c, int grey_state);

  void                anti_twitter                     (WimpRedrawWindowBlock * r);

  int                 adjust                           (void);

  int                 hide_gadget                      (ObjectId o, ComponentId c);
  int                 show_gadget                      (ObjectId o, ComponentId c);
  int                 gadget_hidden                    (ObjectId o, ComponentId c);
  void                slab_gadget                      (ObjectId o, ComponentId c);
  void                slab_gadget_in                   (ObjectId o, ComponentId c);
  void                slab_gadget_out                  (ObjectId o, ComponentId c);

  ObjectId            utils_check_caret_restoration    (ObjectId window_id);
  _kernel_oserror   * utils_restore_caret              (ObjectId window_id);

  _kernel_oserror   * copy_toolaction_info             (ObjectId src_o, ComponentId src_c, ObjectId dst_o, ComponentId dst_c);
  _kernel_oserror   * set_window_flags                 (int window_handle, unsigned int clear_word, unsigned int eor_word);

  int                 debounce_keypress                (void);

  int                 utils_task_from_window           (int window_handle);
  _kernel_oserror   * utils_browser_from_window        (int window_handle, browser_data ** browser);
  int                 is_known_browser                 (const browser_data * restrict b);

  browser_data      * utils_parent                     (const browser_data * restrict b);
  browser_data      * utils_flat_parent                (const browser_data * b);
  browser_data      * utils_ancestor                   (const browser_data * restrict b);

  int                 utils_encode_base64              (const char * in, int len, char * out);
  int                 utils_strcasecmp                 (const char * a, const char * b);
  int                 utils_strncasecmp                (const char * a, const char * b, unsigned int n);
  int                 utils_strnullcmp                 (const char * a, const char * b);
  char              * utils_strdup                     (const char * s1);
  int                 utils_len_printf                 (const char * format, ...);
  int                 utils_number_length              (int number);
  unsigned int        utils_return_hash                (const char * s);

  _kernel_oserror   * utils_get_task_handle            (const char * task_to_get, unsigned int * found_handle);
  _kernel_oserror   * utils_stop_proxy                 (void);
  void                utils_build_user_agent_string    (int netscape, char * buffer, int buffer_size);

  int                 utils_check_scrap                (void);
  _kernel_oserror   * utils_canonicalise_path          (const char * in, char ** out);
  _kernel_oserror   * utils_build_tree                 (const char * path);

  void                utils_hourglass_percent          (unsigned int current, unsigned int max);

#endif /* Browser_Utils__ */
