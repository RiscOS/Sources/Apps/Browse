/* Copyright 1997 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**************************************************************/
/* File:    FetchHTML.h                                       */
/*          (C) 2000 Pace Micro Technology PLC                */
/*          All rights reserved                               */
/*                                                            */
/* Purpose: Fetch functions that deal with HTMLLib data       */
/*          (getting more of it, freeing it, and so forth).   */
/*          Compare with higher higher level Fetch.h and      */
/*          FetchPage.h.                                      */
/*                                                            */
/* Author:  A.D.Hodgkinson.                                   */
/*                                                            */
/* History: 17-Aug-1997 (ADH): Created.                       */
/*          10-Apr-2000 (ADH): 64-wide comments adopted.      */
/**************************************************************/

#ifndef Browser_FetchHTML__
  #define Browser_FetchHTML__

  #include <kernel.h>
  #include <flex.h>
  #include <HTMLLib/HTMLLib.h>

  #include "Global.h"

  /* Function prototypes */

  _kernel_oserror * html_get            (char * url, char ** extradata, int * handle, int method, char * user, int allowparse, int proxy, const char * referer);
  int               html_insert_header  (const char * header, flex_ptr data);
  _kernel_oserror * html_close          (int handle);
  _kernel_oserror * html_get_next_token (browser_data * b, unsigned int handle, int * remaining, int * size, HStream ** token, int * waiting, flex_ptr store, char * url, int image);

#endif /* Browser_FetchHTML__ */
